import java.io.*;
class Demo{
	public static void main(String args[])throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter the number");
		int n = Integer.parseInt(br.readLine());
		int sum=0;
		
		while(n>0){
			int rem=n%10;
			int fact=1;
			while(rem>0){
				fact=rem*fact;
				rem--;
			}
			sum = sum+fact;
			n=n/10;
		}
		System.out.println(sum);
	}
}

