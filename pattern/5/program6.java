import java.io.*;
class Demo{
	public static void main(String args[])throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter the first character");
		char ch1 =(char)br.read();
		br.skip(1);
		System.out.println("Enter the second character");
		char ch2 =(char)br.read();
		br.skip(1);
		
		if(ch1==ch2){
			System.out.println(ch1 + ch2);
		}
		else{
			System.out.println(ch2 - ch1);
		}
	}
}
