import java.io.*;
class Demo{
	public static void main(String args[])throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter the number of rows");
		int row = Integer.parseInt(br.readLine());
		int n=15;
		char ch='O';

		for(int i=1;i<=row;i++){

			for(int j=1;j<=i;j++){
				if(i%2==1){
					System.out.print(ch+" ");
				}
				else{
					System.out.print(n+" ");
				}
				ch--;
				n--;
			}
			System.out.println();
		}
	}
}



