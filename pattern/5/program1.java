import java.io.*;
class PatternDemo{
	public static void main(String args[])throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter the number of rows");
		int row = Integer.parseInt(br.readLine());
		int col = 4;
		int n=4;
		char ch='D';

		for(int i=1;i<=row;i++){
			for(int j=1;j<=col;j++){
				if(i%2==1){
				System.out.print(ch);
				System.out.print(n+" ");
				ch--;
				n--;
				}
				else{
				ch++;
				n++;
				System.out.print(ch);
				System.out.print(n+" ");
				}
			}
			System.out.println();
		}

	}
}
