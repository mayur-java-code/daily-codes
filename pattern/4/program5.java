/* A B C D
 * B C D
 * C D
 * D*/
class Demo{
	public static void main(String args[]){
		int row=4;
		char ch1='A';
		for(int i=1;i<=row;i++){
			char ch=ch1;
			for(int j=1;j<=row-i+1;j++){
				System.out.print(ch+" ");
				ch++;
			}
			ch1++;
			System.out.println();
		}
	}
}

