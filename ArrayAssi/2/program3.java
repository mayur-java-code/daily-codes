// print the sum of odd number and the sum of even number from the array element
import java.io.*;
class Demo{
	public static void main(String args[])throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter size of array");
		int size = Integer.parseInt(br.readLine());
		int arr[] = new int[size];
		System.out.println("Enter array element");
		int sum1 = 0;
		int sum2 = 0;
		for(int i=0;i<arr.length;i++){
			arr[i] = Integer.parseInt(br.readLine());
			if(arr[i]%2==0){
				sum1=sum1+arr[i];
			}
			else{
				sum2=sum2+arr[i];
			}
		}
		System.out.println("Even Sum is : "+sum1);
		System.out.println("odd Sum is : "+sum2);
	}
}
