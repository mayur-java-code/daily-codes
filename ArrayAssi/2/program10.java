// WAP to print the elements whose addition of digit is even

import java.io.*;
class Demo{
	public static void main(String args[])throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter the size of array");
		int size=Integer.parseInt(br.readLine());
		int arr[]=new int[size];
		System.out.println("eneter array element");
		for(int i=0;i<arr.length;i++){
			arr[i]=Integer.parseInt(br.readLine());
		}
		int sum=0;
		int temp;
		System.out.println("element whose addition of digit is even:");
		for(int i=0;i<arr.length;i++){
			temp=arr[i];
			while(arr[i]>0){
				int rem=arr[i]%10;
				sum=sum+rem;
				arr[i]=arr[i]/10;
			}
			
			if(sum%2==0){
				System.out.println(temp);
			}
			sum=0;
		}
	}
}
