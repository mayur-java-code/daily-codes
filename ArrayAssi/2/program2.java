//print the count of even and odd
import java.io.*;
class Demo{
	public static void main(String args[])throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter size of array");
		int size = Integer.parseInt(br.readLine());
		int arr[] = new int[size];
		System.out.println("Enter array element");
		int count1 = 0;
		int count2 = 0;
		for(int i=0;i<arr.length;i++){
			arr[i] = Integer.parseInt(br.readLine());
		}
		for(int i=0;i<arr.length;i++){
			if(arr[i]%2==0){
				count1++;
			}
			else{
				count2++;
			}
		}
		System.out.println("Even count is : "+count1);
		System.out.println("odd count is : "+count2);
	}
}
