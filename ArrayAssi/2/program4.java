// WAP to search a specific elemnt from the array and return its index
import java.util.*;
class Demo{
	public static void main(String args[]){
		int arr[]=new int[]{1,2,4,5,6};
		System.out.println("Enter the element which you have to search");
		Scanner sc=new Scanner(System.in);
		int n=sc.nextInt();
		for(int i=0;i<arr.length;i++){
			if(arr[i]==n){
				System.out.println("The return index is :"+i);
			}
		}
	}
}

