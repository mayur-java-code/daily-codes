// take 7 character and print only vowels from the array

import java.io.*;
class Demo{
	public static void main(String args[])throws IOException{
	BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
	System.out.println("Enter size of array");
	int size =Integer.parseInt(br.readLine());
	char arr[] = new char[size];
	System.out.println("enter array element");
	for(int i=0;i<arr.length;i++){
		arr[i]=(char)br.read();
		br.skip(1);
	}
	System.out.println("The vowels is");
	for(int i=0;i<arr.length;i++){
		if(arr[i]=='a'||arr[i]=='e'||arr[i]=='i'||arr[i]=='o'||arr[i]=='u'){
			System.out.print(arr[i]+" ");
	
		}
	}
}
}
