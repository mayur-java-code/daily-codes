// print product of odd index

import java.util.*;
class Demo{
	public static void main(String args[]){
	Scanner sc = new Scanner(System.in);
	System.out.println("Enter size of array");
	int size = sc.nextInt();
	int arr[] = new int[size];
	System.out.println("enter array element");
	for(int i=0;i<arr.length;i++){
		arr[i]=sc.nextInt();
	}
	int mult=1;
	for(int i=0;i<arr.length;i++){
		if(i%2==1){
			mult=mult*arr[i];
		}
	}
	System.out.println("product of odd index  is: "+mult);
}
}
