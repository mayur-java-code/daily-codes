// print sum of odd element of array
import java.util.*;
class Demo{
	public static void main(String args[]){
	Scanner sc = new Scanner(System.in);
	System.out.println("Enter size of array");
	int size = sc.nextInt();
	int arr[] = new int[size];
	System.out.println("enter array element");
	for(int i=0;i<arr.length;i++){
		arr[i]=sc.nextInt();
	}
	int sum=0;
	for(int i=0;i<arr.length;i++){
		if(arr[i]%2==1){
			sum=sum+arr[i];
		}
	}
	System.out.println("sum of odd element is: "+sum);
}
}
