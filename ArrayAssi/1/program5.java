
// print element that are divisible by 5
import java.util.*;
class Demo{
	public static void main(String args[]){
	Scanner sc = new Scanner(System.in);
	System.out.println("Enter size of array");
	int size = sc.nextInt();
	int arr[] = new int[size];
	System.out.println("enter array element");
	for(int i=0;i<arr.length;i++){
		arr[i]=sc.nextInt();
	}
	System.out.println("The element are: ");
	for(int i=0;i<arr.length;i++){
		if(arr[i]%5==0){
			System.out.println(arr[i]);
		}
	}
}
}
