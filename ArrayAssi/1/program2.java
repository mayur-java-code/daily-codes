// print product of even element
import java.util.*;
class Demo{
	public static void main(String args[]){
	Scanner sc = new Scanner(System.in);
	System.out.println("Enter size of array");
	int size = sc.nextInt();
	int arr[] = new int[size];
	System.out.println("enter array element");
	for(int i=0;i<arr.length;i++){
		arr[i]=sc.nextInt();
	}
	int mul=1;
	for(int i=0;i<arr.length;i++){
		if(arr[i]%2==0){
			mul = mul*arr[i];
		}
	}
	System.out.println("multiplication of even element is: "+mul);
}
}
