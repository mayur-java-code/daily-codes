//WAP that takes the cost price and selling price and calculates its profit or loss
class Profit{
	public static void main(String args[]){
		int costprice = 10;
		int sellingprice = 15;
		if(sellingprice > costprice){
			System.out.println("profit of "+sellingprice - costprice);
		}
		else if(costprice > sellingprice){
			System.out.println("loss of "+costprice - sellingprice);
		}
		else{
			System.out.println("no profit no loss");
		}
	}
}

