//Given an int value as i/p
//print fizz if it is divisible by 3
//print buzz if it is divisile by 5
//print fizz-buzz if it is divisible by both
//print "not divisible by both " if not

class Divisible{
	public static void main(String args[]){
		int x=10;
		if(x%3==0 && x%5==0){
			System.out.println("fizz-buzz");
		}
		else if(x%3==0){
			System.out.println("fizz");
		}
		else if(x%5==0){
			System.out.println("buzz");
		}
		else{
			System.out.println("not divisible by both");
		}
	}
}
