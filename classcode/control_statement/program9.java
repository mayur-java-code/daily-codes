//Take an int N as input print odd integer from 1 to N using loop
//input - 10
//output - 1 3 5 7 9

class Loop{
	public static void main(String args[]){
		int i = 1;
		int N = 10;
		while(i<=N){
			if(i%2==1){
			System.out.println(i);
			}
			i++;
		}
	}
}

