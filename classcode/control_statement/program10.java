//Take an int N as input print multiple of 4 till N
//input -22
//output - 4 8 12 16 20
 

class Loop{
	public static void main(String args[]){
		int i=1;
		int n=22;
		while(i<=n){
			if(i%4==0){
				System.out.println(i);
			}
			i++;
		}
	}
}
