//Given the temp of person in faranite print wheather the person has high, normal or low temp
//temp>98.6 - high
//temp<98.0 - low
//temp>98.0 and temp<98.6 - normal

class Temp{
	public static void main(String args[]){
		float temp = 98.3f;
		if(temp>98.6f){
			System.out.println("High");
		}
		else if(temp<98.0f){
			System.out.println("Low");
		}
		else{
			System.out.println("Normal");
		}
	}
}

