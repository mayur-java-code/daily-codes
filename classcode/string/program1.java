class StringDemo{
	public static void main(String args[]){
		String str1 = "Mayur";
		String str2 = new String("Mayur");
		char str3[] = {'M','a','y','u','r'};

		System.out.println(str1);
		System.out.println(str2);
		System.out.println(str3);
	}
}
