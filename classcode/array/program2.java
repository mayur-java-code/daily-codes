//  Print sum of elemnt of array

import java.io.*;
class Demo{
	public static void main(String args[])throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter the Size of array");
		int size = Integer.parseInt(br.readLine());
		int arr[] = new int[size];
		int sum = 0;
		System.out.println("Enter array element");
		for(int i=0;i<arr.length;i++){
			arr[i]=Integer.parseInt(br.readLine());
			sum = sum+arr[i];
		}
		System.out.println("sum is : "+sum);
	}
}
